<?php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="course_student")
*/
class Course_Student
{

     /**
     * @ORM\Id 
     * @ORM\ManyToOne(targetEntity="Course") 
     * @ORM\JoinColumn(name="course_id", referencedColumnName="id", nullable=false) 
     */
    private $course;
    /** 
     * @ORM\Id  
     * @ORM\ManyToOne(targetEntity="Student") 
     * @ORM\JoinColumn(name="student_id", referencedColumnName="id", nullable=false) 
     */
    private $student;
    
    /**
    * @ORM\Column(type="decimal", scale=2)
    */
    protected $grade;

    /**
    * @ORM\Column(type="decimal", scale=2)
    */
    protected $attendance;

       public function __construct($course, $student)
    {
        $this->course = $course;
        $this->student = $student;
    }

  
}
