<?php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="course_room_semester")
*/
class Course_Room_Semester
{

     /**
     * @ORM\Id 
     * @ORM\ManyToOne(targetEntity="Course") 
     * @ORM\JoinColumn(name="course_id", referencedColumnName="id", nullable=false) 
     */
    private $course;
    /** 
     * @ORM\Id  
     * @ORM\ManyToOne(targetEntity="Room") 
     * @ORM\JoinColumn(name="room_id", referencedColumnName="id", nullable=false) 
     */
    private $room;
      /**
       * @ORM\Id  
       * @ORM\ManyToOne(targetEntity="Semester") 
       * @ORM\JoinColumn(name="semester_id", referencedColumnName="id", nullable=false) 
       */
    private $semester;
   

       public function __construct($course, $room, $semester)
    {
        $this->course = $course;
        $this->room = $room;
        $this->semester = $semester;
    }

    /**
    * @ORM\Column(type="string", length=20)
    */
    protected $day;
    /** 
    * @ORM\Column(type="time") 
    */
    protected $from;
    /**
    * @ORM\Column(type="time")
    */
    protected $to;
}
