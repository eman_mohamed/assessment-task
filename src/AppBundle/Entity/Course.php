<?php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="course")
*/
class Course
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;
	/**
	* @ORM\Column(type="string", length=100)
	*/
	protected $name;

	/**
	* @ORM\Column(type="decimal", scale=2)
	*/
	protected $total_grade;
    
    /**
    * @ORM\Column(type="integer")
    */
    protected $max_students = 50;


	public function __construct()
	{
        $this->students = new \Doctrine\Common\Collections\ArrayCollection();
    }
 
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Course
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set total_grade
     *
     * @param string $totalGrade
     * @return Course
     */
    public function setTotalGrade($totalGrade)
    {
        $this->total_grade = $totalGrade;

        return $this;
    }

    /**
     * Get total_grade
     *
     * @return string 
     */
    public function getTotalGrade()
    {
        return $this->total_grade;
    }
}
