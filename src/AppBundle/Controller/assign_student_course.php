<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
class DefaultController extends Controller
{
    
    /**
     * @Route("/assigncourses", name="assigncourses")
     * @Template("AppBundle:assign_student_course:index.html.twig")
     */
    public function indexAction(Request $request)
    {
      
    }
}
